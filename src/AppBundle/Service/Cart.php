<?php

namespace AppBundle\Service;
use Symfony\Component\HttpFoundation\RequestStack;
use Doctrine\ORM\EntityManager;

class Cart
{
	protected $em;
	public function __construct(EntityManager $em, RequestStack $requestStack)
	{
		$this->em = $em;
		$this->requestStack = $requestStack;
	}
	
	public function getProducts()
	{
		$cart = $this->getCart();
		$products=[];
		$productRepo = $this->em->getRepository('AppBundle:Product');
		if (!empty($cart['products']))
		{
			foreach( $cart['products'] as $product)
				$products[] = $productRepo->findOneBy(array('id'=>$product['id']));
		}
		
		if($products)
			return $products;
		else
			return false;
	}
	
	public function getCart()
	{
		$request = $this->requestStack->getCurrentRequest();
		$session = $request->getSession();
		$cart    = $session->get("cart") ? $session->get("cart") : array();
		return $cart;
	}
	
	public function saveCart($cart)
	{
		$request = $this->requestStack->getCurrentRequest();
		$session = $request->getSession();
		
		$cart['total'] = 0;
		$cart['totalPrice'] = 0;
		if(!empty($cart['products']))
		{
			
			foreach ($cart['products'] as $product){
				$cart['total'] += $product['quantity'];
				$cart['totalPrice'] += $product['quantity'] * $product['price'];
			}
		}
//		$cart = [];
		$session->set("cart",$cart);
	}
	
	public function addProduct($productId)
	{
		$request = $this->requestStack->getCurrentRequest();
		$session = $request->getSession();
		$cart = $this->getCart();
		$product = $this->em
			->getRepository('AppBundle:Product')
			->findOneBy(array('id'=>$productId));
//		$productId = $request->request->get('id');
		$productPrice = $product->getPrice();
//		$cart = [];
		
		
		if(empty($cart['products'])){
			$cart = [];
			$cart['total'] = 1;
			$cart['totalPrice'] = $productPrice;
			$cart['products'] = [];
			
			$cart['products'][$productId] = array(
				'id' => $productId,
				'price' => $productPrice,
				"quantity" => 1,
			
			);
		}
		elseif (empty($cart['products'][$productId]))
		{
			$cart['products'][$productId] = array(
				'id' => $productId,
				'price' => $productPrice,
				"quantity" => 1,
			
			);
		}
		else
		{
			echo "Adding<br>";
			$cart['products'][$productId] = array(
				'id' => $productId,
				'price' => $productPrice,
				"quantity" => $cart['products'][$productId]['quantity']+1,
			
			);
			$cart['total'] = 0;
			$cart['totalPrice'] = 0;
			foreach ($cart['products'] as $product){
				$cart['total'] += $product['quantity'];
				$cart['totalPrice'] += $product['quantity'] * $product['price'];
			}
		}
		
		//$cart[$productId] = $productId;
		asort($cart);
		
		//$cart = [];
		print_r($cart);
		$this->saveCart($cart);
		die('123');
		
		//$session->set("cart",$cart);
		//print_r($session->get("cart"));
		
	}
	
	
	public function removeProduct($productId)
	{
		
		$request = $this->requestStack->getCurrentRequest();
		//$session = $request->getSession();
		$cart = $this->getCart();
		
		if(!empty($cart['products']) && !empty($cart['products'][$productId]))
		{
			unset($cart['products'][$productId]);
			
			$this->saveCart($cart);
		}
		//print_r($cart);
		die('hard');
	}
	
	public function changeCart()
	{
		//$this->cleanCart();
		$cart = $this->getCart();
		$request = $this->requestStack->getCurrentRequest();
		
		if($request->request->get('action') == 'add' && $request->request->get('productId')){
			$this->addProduct($request->request->get('productId'));
			//die('add');
		}
		
		if($request->request->get('action') == 'remove' && $request->request->get('productId')){
			$this->removeProduct($request->request->get('productId'));
			//die('remove');
		}
		
		$changeProperty = $request->request->get('changeProperty');
		$propertyValue = $request->request->get('propertyValue');
		$productId = $request->request->get('productId');
		
		switch ($changeProperty){
			case "quantity":
				if(!empty($cart['products']) && !empty($cart['products'][$productId])){
					
					$cart['products'][$productId][$changeProperty] = $propertyValue;
					echo $request->request->get('propertyValue');
					break;
				}
		}
		
		//Updating quantity and total price
		$cart['total'] = 0;
		$cart['totalPrice'] = 0;
		foreach ($cart['products'] as $product){
			$cart['total'] += $product['quantity'];
			$cart['totalPrice'] += $product['quantity'] * $product['price'];
		}
		
		$this->saveCart($cart);
	}
	
	public function cleanCart()
	{
		$cart = [];
		$this->saveCart($cart);
	}
	
}