<?php

namespace AppBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="product")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductRepository")
 */

class Product
{
	
	/**
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;
	
	/**
	 * @ORM\Column(type="string", length=100, nullable=true)
	 */
	private $name;
	
	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $path;
	
	/**
	 * @Assert\Length(
	 *      min = 3,
	 *      max = 40,
	 *      minMessage = "Your first name must be at least {{ limit }} characters long",
	 *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
	 * )
	 * @var string
	 *
	 * @ORM\Column(type="string", length=80, nullable=true, name="code")
	 */
	private $code;
	
	/**
	 * @ORM\Column(type="decimal", nullable=true, scale=2)
	 */
	private $price;
	
	/**
	 * @ORM\Column(type="integer", nullable=true)
	 */
	private $rating;
	
	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $description;
	
	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $descriptionShort;
	
	/**
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Category", inversedBy="products")
	 * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
	 */
	private $category;
	
	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}
	
	/**
	 * Set name
	 *
	 * @param string $name
	 * @return Product
	 */
	public function setName($name)
	{
		$this->name = $name;
		
		return $this;
	}
	
	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}
	
	/**
	 * Set path
	 *
	 * @param string $path
	 * @return Product
	 */
	public function setPath($path)
	{
		$this->path = $path;
		
		return $this;
	}
	
	/**
	 * Get path
	 *
	 * @return string
	 */
	public function getPath()
	{
		return $this->path;
	}
	
	/**
	 * Set price
	 *
	 * @param string $price
	 * @return Product
	 */
	public function setPrice($price)
	{
		$this->price = $price;
		
		return $this;
	}
	
	/**
	 * Get price
	 *
	 * @return string
	 */
	public function getPrice()
	{
		return $this->price;
	}
	
	/**
	 * Set description
	 *
	 * @param string $description
	 * @return Product
	 */
	public function setDescription($description)
	{
		$this->description = $description;
		
		return $this;
	}
	
	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}
	
	/**
	 * Set descriptionShort
	 *
	 * @param string $descriptionShort
	 * @return Product
	 */
	public function setDescriptionShort($descriptionShort)
	{
		$this->descriptionShort = $descriptionShort;
		
		return $this;
	}
	
	/**
	 * Get descriptionShort
	 *
	 * @return string
	 */
	public function getDescriptionShort()
	{
		return $this->descriptionShort;
	}
	
	/**
	 * Set category
	 *
	 * @param \AppBundle\Entity\Category $category
	 * @return Product
	 */
	public function setCategory(\AppBundle\Entity\Category $category = null)
	{
		$this->category = $category;
		
		return $this;
	}
	
	/**
	 * Get category
	 *
	 * @return \AppBundle\Entity\Category
	 */
	public function getCategory()
	{
		return $this->category;
	}
	
	/**
	 * Set code
	 *
	 * @param string $code
	 * @return Product
	 */
	public function setCode($code)
	{
		$this->code = $code;
		
		return $this;
	}
	
	/**
	 * Get code
	 *
	 * @return string
	 */
	public function getCode()
	{
		return $this->code;
	}

	/**
	 * Set rating
	 *
	 * @param string $rating
	 * @return Product
	 */
	public function setRating($rating)
	{
		$this->rating = $rating;
		
		return $this;
	}
	
	/**
	 * Get rating
	 *
	 * @return string
	 */
	public function getRating()
	{
		return $this->rating;
	}
	
}
