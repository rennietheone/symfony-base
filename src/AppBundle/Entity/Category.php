<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Category
 * @Gedmo\Tree(type="nested")
 * @ORM\Table(name="category")
 * use repository for handy tree functions
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoryRepository")
 * @UniqueEntity("code")
 * @UniqueEntity("name")
 */

class Category
{
	/**
	 * @var int
	 *
	 * @ORM\Column(type="integer", name="id")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;
	
	/**
	 * @var string
	 * @Assert\Length(
	 *      min = 3,
	 *      max = 255,
	 *      minMessage = "Your name first name must be at least {{ limit }} characters long",
	 *      maxMessage = "Your name cannot be longer than {{ limit }} characters"
	 * )
	 * @ORM\Column(type="string", unique=true, length=255, nullable=true, name="name")
	 */
	private $name;
	
	/**
	 * @var string
	 * @ORM\Column(type="string", unique=true, nullable=true, name="path")
	 */
	private $path;
	
	/**
	 * @var string
	 * @Assert\Length(
	 *      min = 3,
	 *      max = 40,
	 *      minMessage = "Your code first name must be at least {{ limit }} characters long",
	 *      maxMessage = "Your code cannot be longer than {{ limit }} characters"
	 * )
	 * @ORM\Column(type="string", unique=true, length=80, nullable=true, name="code")
	 */
	private $code;
	
	/**
	 * @Gedmo\TreeLeft
	 * @ORM\Column(type="integer", unique=true, nullable=false)
	 */
	private $lft;
	
	/**
	 * @Gedmo\TreeLevel
	 * @ORM\Column(type="integer", nullable=true)
	 */
	private $lvl;
	
	/**
	 * @Gedmo\TreeRight
	 * @ORM\Column(type="integer", nullable=true)
	 */
	private $rgt;
	
	/**
	 *
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Category")
	 * @ORM\JoinColumn(name="root_id", referencedColumnName="id", onDelete="CASCADE")
	 */
	private $root;
	
	/**
	 * @Gedmo\TreeParent
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Category", inversedBy="children")
	 * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
	 */
	private $parent;
	
	/**
	 * @ORM\OneToMany(targetEntity="AppBundle\Entity\Category", mappedBy="parent")
	 * @ORM\OrderBy({"lft":"ASC"})
	 */
	private $children;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(type="text", nullable=true, name="description")
	 */
	private $description;
	
	
	/**
	 * @ORM\OneToMany(targetEntity="AppBundle\Entity\Product", mappedBy="category")
	 */
	private $products;
	
	
	public function __construct()
	{
		$this->products = new ArrayCollection();
	}
	
	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}
	
	/**
	 * Set name
	 *
	 * @param string $name
	 * @return Category
	 */
	public function setName($name)
	{
		$this->name = $name;
		
		return $this;
	}
	
	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}
	
	
	/**
	 * Set path
	 *
	 * @param string $path
	 * @return Category
	 */
	public function setPath($path)
	{
		$this->path = $path;
		
		return $this;
	}
	
	/**
	 * Get path
	 *
	 * @return string
	 */
	public function getPath()
	{
		return $this->path;
	}
	
	/**
	 * Set description
	 *
	 * @param string $description
	 * @return Category
	 */
	public function setDescription($description)
	{
		$this->description = $description;
		
		return $this;
	}
	
	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}
	
	/**
	 * Add products
	 *
	 * @param \AppBundle\Entity\Product $products
	 * @return Category
	 */
	public function addProduct(\AppBundle\Entity\Product $products)
	{
		$this->products[] = $products;
		
		return $this;
	}
	
	/**
	 * Remove products
	 *
	 * @param \AppBundle\Entity\Product $products
	 */
	public function removeProduct(\AppBundle\Entity\Product $products)
	{
		$this->products->removeElement($products);
	}
	
	/**
	 * Get products
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getProducts()
	{
		return $this->products;
	}
	
	/**
	 * Set code
	 *
	 * @param string $code
	 * @return Category
	 */
	public function setCode($code)
	{
		$this->code = $code;
		
		return $this;
	}
	
	/**
	 * Get code
	 *
	 * @return string
	 */
	public function getCode()
	{
		return $this->code;
	}
	
	/**
	 * Set lft
	 *
	 * @param integer $lft
	 * @return Category
	 */
	public function setLft($lft)
	{
		$this->lft = $lft;
		
		return $this;
	}
	
	/**
	 * Get lft
	 *
	 * @return integer
	 */
	public function getLft()
	{
		return $this->lft;
	}
	
	/**
	 * Set lvl
	 *
	 * @param integer $lvl
	 * @return Category
	 */
	public function setLvl($lvl)
	{
		$this->lvl = $lvl;
		
		return $this;
	}
	
	/**
	 * Get lvl
	 *
	 * @return integer
	 */
	public function getLvl()
	{
		return $this->lvl;
	}
	
	/**
	 * Set rgt
	 *
	 * @param integer $rgt
	 * @return Category
	 */
	public function setRgt($rgt)
	{
		$this->rgt = $rgt;
		
		return $this;
	}
	
	/**
	 * Get rgt
	 *
	 * @return integer
	 */
	public function getRgt()
	{
		return $this->rgt;
	}
	
	/**
	 * Set root
	 *
	 * @param \AppBundle\Entity\Category $root
	 * @return Category
	 */
	public function setRoot(\AppBundle\Entity\Category $root = null)
	{
		$this->root = $root;
		
		return $this;
	}
	
	/**
	 * Get root
	 *
	 * @return \AppBundle\Entity\Category
	 */
	public function getRoot()
	{
		return $this->root;
	}
	
	/**
	 * Set parent
	 *
	 * @param \AppBundle\Entity\Category $parent
	 * @return Category
	 */
	public function setParent(\AppBundle\Entity\Category $parent = null)
	{
		$this->parent = $parent;
		
		return $this;
	}
	
	/**
	 * Get parent
	 *
	 * @return \AppBundle\Entity\Category
	 */
	public function getParent()
	{
		return $this->parent;
	}
	
	/**
	 * Add children
	 *
	 * @param \AppBundle\Entity\Category $children
	 * @return Category
	 */
	public function addChild(\AppBundle\Entity\Category $children)
	{
		$this->children[] = $children;
		
		return $this;
	}
	
	/**
	 * Remove children
	 *
	 * @param \AppBundle\Entity\Category $children
	 */
	public function removeChild(\AppBundle\Entity\Category $children)
	{
		$this->children->removeElement($children);
	}
	
	/**
	 * Get children
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getChildren()
	{
		return $this->children;
	}
	
	public function __toString() {
		
		return (string) $this->getCode();
//		return 'string';
	}
}
