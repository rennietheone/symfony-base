<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Gedmo\Tree\Traits\NestedSetEntity;

/**
 * CategoryRepository
 */
class CategoryRepository extends \Gedmo\Tree\Entity\Repository\NestedTreeRepository
{
	use NestedSetEntity;
	
	public function getUrlById($id){
		$id = intval($id);
		$path = implode('/', $this->getPath($this->findOneBy(array('id' =>$id))));
		return $path;
	}
	
	public function getBreadcrumbs($category){
		
		$breadcrumbs = [];
		$path = $this->getPath($category);
		$text = '/catalog/';
		foreach ($path as $pathPart){
			if( substr($text, -1) != '/')
				$text .= '/';
			$text .= $pathPart->getCode();
			$breadcrumbs[$text] = $pathPart->getName();
		}
		
		return $breadcrumbs;
	}
	
	public function getTree(){
		$repo = $this;
		$options = array(
			'decorate' => true,
			'rootOpen' => function ($tree) {
				if (count($tree) && ($tree[0]['lvl'] == 0)) {
					return '<div class="col-lg-3 left-menu-wrapper">
	<ul class="nav nav-pills nav-stacked catalog-left-menu">';
				}
			},
			'rootClose' => function ($tree) {
				if (count($tree) && ($tree[0]['lvl'] == 0)) {
					return '</ul></div>';
				}
			},
			'childOpen' => function ($child) use ($repo) {
				
				if (count($child) && ($child['lvl'] == 0)) {
					//echo "=open=";
					return
						'<li><a href="#"><span class="glyphicon glyphicon-chevron-down"></span>'
						. $child["name"]
						. '</a><div class="hidden-content">';
				}
				$text = '';
				if (count($child) && ($child['lvl'] != 0)) {
//						echo "=open=".$child['lvl'].$child['name']."<br>";
					if (!empty($child['__children'][0])) {
						$url = $repo->getUrlById($child['id']);
						//echo "=parent=".$child['lvl'].$child['name']."<br>";
						$text = '<span class="item-with-children-plus">'
							. '<i class="fa fa-plus-square-o" aria-hidden="true"></i>'
							. '</span><a class="left-menu" href="/catalog/'
							.$url.'">'
							. $child['name'] . '</a><ul class="inner-ul">';
					} else {
						
						$text = '<a href="/catalog/'.$repo->getUrlById($child['id']).'">' . $child['name'] . '</a>';
						if($child['lvl'] > 1){
							$text = '<li><a href="/catalog/'
								.$repo->getUrlById($child['id'])
								.'">' . $child['name'] . '</a></li>';
						}
						
					}
				}
				return $text;
			},
			'childClose' => function ($child) {
				$text = '';
				if (count($child) && $child['lvl'] == 0)
					$text = '</div></li>';
				if (!empty($child['__children'][0]) && $child['lvl'] != 0) {
					$text = "</ul>";
				}
				return $text;
			}
		,
			'nodeDecorator' => function ($node) use ($repo) {
			}
		);
		$tree = $repo->childrenHierarchy(null, false, $options);
		return $tree;
	}
	
	/**
	 * Returns object or void
	 * @param string $url
	 *
	 * @return object|bool
	 */
	public function checkUrlForObject($url){
		$initialUrl = $url;
		$url = array_filter(explode('/', $url));
		$lastUrlPart = array_pop($url);
		$category = $this->findOneByCode($lastUrlPart);
		$productRepo = $this
			->getEntityManager()
			->getRepository('AppBundle:Product');
		$product = $productRepo->findOneByCode($lastUrlPart);
		
		if ($category) {
			
			$path = $this->getPath($category);
			$urlArray = [];
			foreach ($path as $pathPart){
				$urlArray[$pathPart->getCode()] = $pathPart->getCode();
			}

			$urlToCheck = implode('/', $urlArray);
			
			if ($initialUrl == $urlToCheck) {
				
				return $category;

			}
		}
		
		if ($product) {
			
			$categoryOfProduct = $this->findOneByCode(array_pop($url));
			$path = $this->getPath($categoryOfProduct);
			$urlArray = [];
			
			foreach ($path as $pathPart) {
				$urlArray[$pathPart->getCode()] = $pathPart->getCode();
			}
			
			$urlToCheck = implode('/', $urlArray);
			$urlToCheck .= "/" . $product->getCode();
			
			if ($initialUrl == $urlToCheck) {
				
				return $product;
			}
		}
		return false;

	}
	
	public function __toString() {
		return (string) $this->getCode();
	}
}
