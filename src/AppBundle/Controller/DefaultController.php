<?php

namespace AppBundle\Controller;

use AppBundle\Form\ProductType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="startpage")
     */
    public function indexAction(Request $request)
    {

        return $this->render('AppBundle::index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }
	/**
	 * @Route("/product", name="product")
	 */
	public function productAction(Request $request, EntityManagerInterface $em)
	{
		$product = new Product;

        $form = $this->createForm(ProductType::class, $product);
		$form->handleRequest($request);
		
		if ($form->isSubmitted() && $form->isValid()) {
			$product = $form->getData();
			$em->persist($product);
			$em->flush();
		}
		
		$products = $em->getRepository('AppBundle:Product')->findAll();
		

		return $this->render('AppBundle::product.html.twig', [
			'form'      => $form->createView(),
			'products'  => $products
		]);
	}
	
	/**
	 * @Route("/category", name="category")
	 */
	public function categoryAction(Request $request)
	{
		
		return $this->render('AppBundle::category.html.twig', [
		
		]);
	}
	
	/**
	 * @Route("/cart", name="cart")
	 */
	public function cartAction(Request $request)
	{
		$cart = $this->container->get('cart');
		$products = $cart->getProducts();
		return $this->render('AppBundle::cart.html.twig',
			array(
				'products' => $products,
				'cart' => $cart->getCart()
			)
		);
	}
	
	
	/**
	 * @Route("/clear", name="clear")
	 */
	public function clearAction(Request $request, EntityManagerInterface $em)
	{

		if($request->query->get('product')){
			$qb = $em->createQueryBuilder();
			$qb->delete('AppBundle:Product', 'p');
			$query = $qb->getQuery();
			$query->getResult();
		}
		
		return $this->redirectToRoute('product');

	}
}
